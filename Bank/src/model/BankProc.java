package model;

import utils.AccountType;

import java.util.LinkedList;

/**
 * Created by Ioan on 08.05.2017.
 */
public interface BankProc {

    public Person addPerson(int id, String email, String name, String telephone, double balance, int acountId, AccountType accountType);

    public Person removePerson(int id);

    public Person getPerson(int id);

    public Person editPerson(int id, String email, String name, String telephone);

    public LinkedList<Account> getAccounts(int id);

    public Person addAccount(int id, double balance, int acountId, AccountType accountType);

    public Account removeAccount(int personId, int accountId);

    public Account getAccount(int personId, int accountId);

    public double deposit(int personId, int accountId, double sum);

    public double withdraw(int personId, int accountId, double sum);
}
