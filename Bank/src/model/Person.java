package model;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Ioan on 08.05.2017.
 */
public class Person implements Observer, Serializable {

    private String name;
    private int id;
    private String telephone;
    private String email;

    public Person(String name, int id, String telephone, String email) {
        this.name = name;
        this.id = id;
        this.telephone = telephone;
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        return id == person.id;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + id;
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println(arg);
    }
}
