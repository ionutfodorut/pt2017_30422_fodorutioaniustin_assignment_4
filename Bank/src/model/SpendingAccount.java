package model;

/**
 * Created by Ioan on 08.05.2017.
 */

public class SpendingAccount extends Account {

    public SpendingAccount(double balance, int id) {
        super(balance, id);
    }

    @Override
    protected double deposit(double sum) {
        if (super.getBalance() + sum < 0)
            return -1;

        super.setBalance(super.getBalance() + sum);
        this.setChanged();
        this.notifyObservers("Deposited " + sum + " to spending account ");
        return super.getBalance();
    }

    @Override
    protected double withdraw(double sum) {
        if (super.getBalance() - sum < 0)
            return -1;

        super.setBalance(super.getBalance() - sum);
        this.setChanged();
        this.notifyObservers("Withdrawn " + sum + " from spending account ");
        return sum;
    }

    public String getType() {
        return "Spending";
    }
}
