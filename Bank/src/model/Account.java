package model;

import java.io.Serializable;
import java.util.Observable;

/**
 * Created by Ioan on 08.05.2017.
 */
public abstract class Account extends Observable implements Serializable {

    private double balance;
    private int id;

    public Account(double balance, int id) {
        this.balance = balance;
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean equals(Account account){
        if (this.balance == account.balance && this.id == account.id) {
            return true;
        }
        return false;
    }

    protected abstract double deposit(double sum);

    protected abstract double withdraw(double sum);

    public abstract String getType();
}
