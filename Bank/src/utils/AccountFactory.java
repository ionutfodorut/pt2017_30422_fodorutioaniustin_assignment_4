package utils;

import model.Account;
import model.SavingsAccount;
import model.SpendingAccount;

import java.io.Serializable;

/**
 * Created by Ioan on 09.05.2017.
 */
public class AccountFactory implements Serializable{

    private double minInterest;
    private double maxInteres;

    public AccountFactory(int minInterest, int maxInteres) {
        this.minInterest = minInterest;
        this.maxInteres = maxInteres;
    }

    public Account getAccount(AccountType accountType) {
        if (accountType == AccountType.SAVINGS) {
            return new SavingsAccount(0, 0);
        }
        if (accountType == AccountType.SPENDING) {
            return new SpendingAccount(0, 0);
        }
        return null;
    }
}
